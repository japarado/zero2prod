use std::future::{ready, Ready};

use actix_session::{Session, SessionExt};
use actix_web::dev::Payload;
use actix_web::{FromRequest, HttpRequest};

use crate::TypedSession;

impl FromRequest for TypedSession
{
    type Error = <Session as FromRequest>::Error;
    type Future = Ready<Result<TypedSession, Self::Error>>;

    fn from_request(req: &HttpRequest, _payload: &mut Payload) -> Self::Future
    {
        ready(Ok(TypedSession(req.get_session())))
    }
}

impl TypedSession
{
    pub fn log_out(self)
    {
        self.0.purge()
    }
}
