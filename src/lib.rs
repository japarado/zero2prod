extern crate core;

use actix_session::Session;
use actix_web::dev::Server;
use uuid::Uuid;

pub mod authentication;
pub mod configuration;
pub mod domain;
pub mod email_client;
pub mod routes;
mod session_state;
pub mod startup;
pub mod telemetry;
mod utils;
mod idempotency;

pub struct TypedSession(Session);

impl TypedSession
{
    const USER_ID_KEY: &'static str = "user_id";

    pub fn renew(&self)
    {
        self.0.renew();
    }

    pub fn insert_user_id(&self, user_id: Uuid) -> Result<(), serde_json::Error>
    {
        self.0.insert(Self::USER_ID_KEY, user_id)
    }

    pub fn get_user_id(&self) -> Result<Option<Uuid>, serde_json::Error>
    {
        self.0.get(Self::USER_ID_KEY)
    }
}
